# Attività 2 - Big Data

### Descrizione

In questa repository sono presenti i file per la consegna dell'attività numero 2 di Big Data, che riguarda l'approfondimento di un aspetto delle architetture Big Data.
Ho deciso di approfondire le librerie di colloquio con il mondo esterno su piattaforme Big Data, in particolare mi sono concentrato su strumenti di Data Ingestion e Data Visualization. 
Ho descritto le caratteristiche e gli usi più comuni di questi strumenti e ho creato due notebook che descrivono un utilizzo pratico di un tool che ho presentato nella relazione.

L'attività principale riguarda la scrittura della relazione "BDA - Attività Big Data" ma è comunque possibile testare il codice dell'esempio pratico. Per fare ciò bisogna scaricare Kafka seguendo le istruzioni presenti [qua](https://kafka.apache.org/quickstart) e bisogna installare le librerie Python "kafka-python", "pymongo" e "beautifulsoup4". Inoltre bisogna installare il software "zookeeper" per il funzionamento di Kafka e creare il topic "stock_prices" attraverso il seguente comando all'interno della cartella di Kafka:
`bin/kafka-topics.sh --create --topic stock_prices --bootstrap-server localhost:9092`

Infine, prima di lanciare i notebook bisogna avviare MongoDB.
